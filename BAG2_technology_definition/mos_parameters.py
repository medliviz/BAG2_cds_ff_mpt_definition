"""
=========================
BAG2 Mos parameter module 
=========================

Collection of MOS transistor related technology parameters for 
BAG2 framework

Initially created by Marko Kosunen, marko.kosunen@aalto.fi, 08.04.2022.

The goal of the class is to provide a master source for the MOS parameters 
and enable documentation of those parameters with docstrings.

Documentation instructions
--------------------------

Current docstring documentation style is Numpy
https://numpydoc.readthedocs.io/en/latest/format.html

"""

import os
import pkg_resources
import yaml

from BAG2_technology_template.mos_parameters_template import mos_parameters_template

class mos_parameters(mos_parameters_template):

    @property
    def mos_pitch(self):
        '''OD quantization pitch.

        '''
        return 48 

    @property
    def analog_unit_fg(self):
        ''' Number of fingers in an AnalogBase row must be multiples of this number.

        '''
        return 2 

    @property
    def draw_zero_extension(self): 
        ''' True if zero height AnalogMosExt should be drawn.
        
        '''
        return False

    @property
    def floating_dummy(self):
        ''' True if floating dummies are allowed.
        
        '''
        return False

    @property
    def abut_analog_mos(self):
        ''' True if AnalogMosConn can abut each other.
        
        '''
        return True

    @property
    def draw_sub_od(self):
        ''' True to draw OD in substrate contact.  False generally used to draw SOI transistors.
        
        '''
        return True
#    #MVM 2check w/ Marko
#    @property
    def sub_ring_lch(self):
        ''' channel length used in substrate rings.
        
        '''
        return 18.0e-9 
    
    @property
    def dum_conn_pitch (self):
        ''' vertical dummy wires pitch, in routing grid tracks.
        
        '''
        return 1 
    
    @property
    def dum_layer(self):
        ''' dummy connection layer.
        
        '''
        return 1 
    
    @property
    def ana_conn_layer(self):
        ''' AnalogBase vertical connection layer.
        
        '''
        return 3 

    @property
    def dig_conn_layer(self):
        ''' LaygoBase vertical connection layer
        
        '''
        return 1 
    
    @property
    def dig_top_layer(self):
        ''' LaygoBase top layer.
        
        '''
        return 2 
    
    @property
    def imp_od_encx(self):
        ''' horizontal enclosure of implant layers over OD
        
        '''
        return 65 
    
    @property
    def od_spy_max(self):
        ''' maximum space between OD rows in resolution units.
        ''' 
       
        return 600 

    @property
    def od_min_density(self):
        ''' minimum OD density
        
        '''
        return 0.20 
    
    @property
    def od_fill_w(self):
        ''' dummy OD height range.
        
        '''
        return (int , int )

    @property
    def md_w(self):
        ''' width of bottom bound-box of OD-METAL1 via
        Defines also x width of the quard ring
        
        '''
        return 40 
#MVM check w/M
    @property
    def od_spy(self):
        ''' minimum vertical space between OD, in resolution units 
        '''
        return 10 

    @property
    def imp_od_ency(self):
        ''' implant layers vertical enclosure of active.
        this is used to figure out where to separate implant layers in extension blocks,
        '''
        return 45 

    @property
    def imp_po_ency(self):
        ''' implant layers vertical enclosure of poly.
        this is used to figure out where to separate implant layers in extension blocks,
        if None, this rule is ignored.
        Does not seem to affect anything
        
        '''
        return int 

    @property
    def nw_dnw_ovl(self):
        ''' overlap between N-well layer and Deep N-well layer.
        
        '''
        return 100 

    @property
    def nw_dnw_ext(self):
        ''' extension of N-well layer over Deep N-well layer.
        
        '''
        return 100 

    @property
    def min_fg_decap(self):
        ''' Minimum number of fingers for decap connection.
        
        '''
        return { 'lch' : [float('inf')],
                 'val' : [2 ]
               }

    @property
    def min_fg_sep(self):
        ''' Minimum number of fingers between separate AnalogMosConn.
        
        '''
        return { 'lch': [float('inf')],
                 'val': [2 ]
               }

    @property
    def edge_margin(self):
      ''' space between AnalogBase implant and boundary.
      
      '''
      return { 'lch' : [float('inf')],
               'val' : [150 ]
             }

    @property
    def fg_gr_min(self):
        ''' minimum number of fingers needed for left/right guard ring.
        
        '''
        return { 'lch': [float('inf')],
                 'val' :  [2 ]
        }

    @property
    def fg_outer_min(self):
        ''' minimum number of fingers in outer edge block.
        
        '''
        return { 'lch': [float('inf')],
                 'val' :  [3 ]
       }

    @property
    def sd_pitch_constants(self):
        ''' source/drain pitch related constants.
        source/drain pitch is computed as val[0] + val[1] * lch_unit

        '''
        return { 'lch': [float('inf')],
                 'val' :  [[90 , 0 ]]
       }

    @property
    def num_sd_per_track(self):
        ''' number of source/drain junction per vertical track.
        
        '''
        return { 'lch': [float('inf')],
                 'val' :  [1 ]
       }

    @property
    def po_spy(self):
        ''' space between PO
        
        '''
        return { 'lch': [float('inf')],
                 'val' :  [0 ]
       }

    @property
    def mx_gd_spy(self):
        ''' vertical space between gate/drain metal wires.
        
        '''
        return { 'lch': [float('inf')],
                 'val' :  [int ]
       }

    @property
    def od_gd_spy(self):
        ''' space between gate wire and OD
        
        '''
        return { 'lch': [float('inf')],
                 'val' :  [int ]
       }

    @property
    def po_od_exty(self):
        ''' PO extension over OD
        
        '''
        return { 'lch': [float('inf')],
                 'val' :  [10 ]
        }


    @property
    def po_od_extx_constants(self):
        ''' OD horizontal extension over PO.
        value specified as a (offset, lch_scale, sd_pitch_scale) tuple, 
        where the extension is computed as 
        offset + lch_scale * lch_unit + sd_pitch_scale * sd_pitch_unit
        
        DEPRECATED: specify a constant number directly.
        (What does this mean)

        '''
        return { 'lch': [float('inf')],
                 'val':  [(0, 0, 1)]
        }

    @property
    def po_h_min(self):
        '''minimum PO height

        '''
        return { 'lch': [float('inf')],
                 'val' :  [int ]
        }

    @property
    def sub_m1_extx(self):
        ''' distance between substrate METAL1 left edge to
        center of left-most source-drain junction.
        
        '''
        return { 'lch': [float('inf')],
                 'val' :  [int ]
        }


    @property
    def sub_m1_enc_le(self):
        ''' substrate METAL1 via line-end enclsoure
        
        '''
        return { 'lch': [float('inf')],
                 'val' :  [int ]
        }

    @property
    def dum_m1_encx(self):
        ''' dummy METAL1 horizontal enclosure.
        
        '''
        return { 'lch': [float('inf')],
                 'val' :  [int ]
        }


    @property
    def g_bot_layer(self):
        ''' gate wire bottom layer ID
        
        '''
        return 1 

    @property
    def d_bot_layer(self):
        ''' drain/source wire bottom layer ID
        
        '''
        return 1 

    @property
    def g_conn_w(self):
        ''' gate vertical wire width on each layer.
        
        '''
        return { 'lch': [float('inf')],
                 'val' : [[40 , 40 , 40 ],
                     ]
               }

    @property
    def d_conn_w(self):
        ''' drain vertical wire width on each layer.
        
        '''
        return { 'lch': [float('inf')],
                 'val': [[40 , 72 , 40 ]
                     ]
               }

    @property
    def g_conn_dir(self):
        ''' gate wire directions
        
        '''
        return ['y', 'x', 'y']


    @property
    def d_conn_dir(self):
        ''' drain wire directions
        
        '''
        return ['y', 'x', 'y']

    @property
    def g_via(self):
        ''' gate via parameters
        
        '''
        return { 'dim' : [ 
            [32 , 32 ],
            [32 , 32 ],
            [32 , 32 ]
            ],
        'sp' : [32 , 32 , 42 ],
        #This is provided not to break old generators
        'bot_enc_le' : [18 , 40 , 34 ],
        #This can be used to control gate poly extension
#        'bot_enc_le_top' : [int , int , int ],
#        'bot_enc_le_bot' : [int , int , int ],
        'top_enc_le' : [40 , 34 , 40 ]
       }


    @property
    def prim_offset(self):
        """Y direction offset for the primitive used in Analog base

        Currently used at Aalto
           
        """
        return int

    @property
    def d_via(self):
        ''' drain/source via parameters
  
        '''
    #Minimum dimension of vias from PO to mos_conn_layer -1 (2)
        return { 'dim': [ 
            [32 , 32 ],
            [32 , 64 ], 
            [32 , 64 ]
        ],
        #Minimum spacing of vias
        'sp' : [32 , 42 , 42 ],
        'bot_enc_le' : [18 , 20 , 10 ],
        'top_enc_le' : [40 , 10 , 20 ]
    }


    @property
    def substrate_contact(self):
        """Substrate contact parameter 
        Copy d_via here if needed. Used Analog Base if 
        substrate contacs differ from drain contacts

        """
        return { 'dim': [ 
            [int, int ],
            [int, int ], 
            [int, int ]
        ],
        #Minimum spacing of vias
        'sp' : [int, int ],
        'bot_enc_le' : [int, int],
        'top_enc_le' : [int, int]
    }


    @property
    def dnw_layers(self):
        ''' Deep N-well layer names
        
        '''
        return [ ('dnw', 'drawing'),
                ]
    @property
    def imp_layers(self):
        ''' implant layer names for each transistor/substrate tap type.
        
        '''
        return { 
         # 'nch_thick' : {
         #     ('layer', 'purpose') : [0,0],
         #     ('layer', 'purpose') : [0,0]
         # },
          'nch' : {
              ('FinArea', 'fin48') : [10, 50]
          },
          'pch': {
               ('FinArea', 'fin48') : [10, 50],
               ('NWell', 'drawing'): [10, 50],
          },
          'ptap' : {
            ('FinArea', 'fin48'): [10, 50]
          },
          'ntap' : {
            ('FinArea', 'fin48') : [10, 50],
            ('NWell', 'drawing'): [10, 50],
          }
    }
##MVM 2 Marko nch_thick not set - probably remove this parameter, abs class- method need to be present
    @property
    def thres_layers(self):
        ''' threshold layer names for each transistor/substrate tap type.
        
        '''
        return { 
            'nch' : {
              'standard' : { 
                  ('Nsvt', 'drawing') : [10, 50]
                  },
              'svt' : {
                  ('Nsvt', 'drawing') : [10, 50]
                  },
              'lvt' : {
                  ('Nlvt', 'drawing') : [10, 50]
                  },
              #'ulvt' : { 
              #    ('layer', 'purpose') : [0, 0]
              #    },
              'fast' : {
                  ('Nlvt', 'drawing') : [10, 50]
                  },
              'hvt' : {
                  ('Nhvt', 'drawing') : [10, 50]
                  },
             # 'uhvt' : {
             #     ('layer', 'purpose') : [0, 0]
             #     }
             },
            'pch' : {
              'standard' : {
                  ('Psvt', 'drawing') : [10, 50]
                  },
              'svt' : {
                  ('Psvt', 'drawing') : [10, 50]
                  },
              'lvt' : {
                  ('Plvt', 'drawing') : [10, 50]
                  },
#              'ulvt' : {
#                  ('layer', 'purpose') : [0, 0]
#                  },
              'fast' : {
                  ('Plvt', 'drawing') : [10, 50]
                  },
              'hvt' : {
                  ('Phvt', 'drawing') : [10, 50]
                  },
              #'uhvt' : {
              #    ('layer', 'purpose') : [0, 0]
              #    },
             },
            'ptap' : {
              'standard' : {
                  ('Psvt', 'drawing') : [10, 50]
                  },
              'svt' : {
                  ('Psvt', 'drawing') : [10, 50]
                  },
              'lvt' : {
                  ('Plvt', 'drawing') : [10, 50]
                  },
              #'ulvt' : {
              #    ('layer', 'purpose') : [0, 0]
              #    },
              'fast' : {
                  ('Plvt', 'drawing') : [10, 50]
                  },
              'hvt' : {
                  ('Phvt', 'drawing') : [10, 50]
                  },
              #'uhvt' : {
              #    ('layer', 'purpose') : [0, 0]
              #    }
            },
            'ntap' : {
              'standard' : {
                  ('Nsvt', 'drawing') : [0, 0]
                  },
              'svt' : {
                  ('Nsvt', 'drawing') : [0, 0]
                  },
              'lvt' : {
                  ('Nlvt', 'drawing') : [0, 0]
                  },
              #'ulvt' : {
              #    ('layer', 'purpose') : [0, 0]
              #    },
              'fast' : {
                  ('Nlvt', 'drawing') : [0, 0]
                  },
              'hvt' : {
                  ('Nhvt', 'drawing') : [0, 0]
                  },
              #'uhvt' : {
              #    ('layer', 'purpose') : [0, 0]
              #    }
            }
        }


    @property
    def fin_h(self):
        '''fin height

        '''
        return 14 

    @property
    def od_spx(self):
        '''minimum horizontal space between OD, in resolution units

        '''
        return 50 

    @property
    def od_fin_exty_constants(self):
        '''Optional: OD vertical extension over fins

        '''
        return [0, 0, 0] 


    @property
    def  od_fin_extx(self):
        '''horizontal enclosure of fins over OD

        '''
        return 216

    @property
    def no_sub_dummy(self):
        '''True if dummies cannot be drawn on substrate region

        '''
        return False 

    @property
    def  mos_conn_modulus(self):
        '''transistor/dummy connections modulus

        '''
        return 1

    @property
    def  od_fill_h(self):
        '''dummy OD height range

        '''
        return [2, 20]

    @property
    def od_fill_w_max(self):
        '''dummy OD maximum width, in resolution units

        '''
        return None 


    @property
    def imp_min_w(self):
        '''minimum implant layer width

        '''
        return 52

    @property
    def  imp_edge_dx(self):
        '''dictionary from implant layers to X-delta in outer edge blocks

        '''
        return  {('pp', 'drawing') : [0, 0, 0]}

# MVM check data format !!python/tuple but with dir
#
    @property
    def mp_h_sub(self):
        '''substrate MP height

        '''
        return 40 

    @property
    def mp_spy_sub(self):
        '''substrate MP vertical space

        '''
        return 34
    
    @property
    def mp_po_ovl_constants_sub(self):
        '''substrate MP extension/overlap over PO

        '''
        return [16, 0] 

    @property
    def mp_md_sp_sub(self):
        '''substrate MP space to MD

        '''
        return 13
    
    @property
    def mp_cpo_sp_sub(self):
        '''substrate MP space to CPO

        '''
        return 19 

    @property
    def mp_h(self):
        '''MP height

        '''
        return 40    
    
    @property
    def mp_spy(self):
        '''vertical space between MP

        '''
        return 34 

    @property
    def mp_po_ovl_constants(self):
        ''' MP and PO overlap

        '''
        return [16, 0]    

    @property
    def mp_md_sp(self):
        '''space bewteen MP and MD

        '''
        return 13 

    @property
    def mp_cpo_sp(self):
        ''' space between MP and CPO

        '''
        return 19    

    @property
    def  has_cpo(self):
        '''True to draw CPO

        '''
        return True
 
    @property
    def cpo_h(self):
        ''' normal CPO height

        '''
        return { 'lch' : [float('inf')],
                 'val' : [60 ]
               }

    @property
    def cpo_po_extx(self):
        ''' horizontal extension of CPO beyond PO

        '''
        return { 'lch' : [float('inf')],
                 'val' : [34 ]
               }

    @property
    def cpo_po_ency(self):
        ''' vertical enclosure of CPO on PO

        '''
        return { 'lch' : [float('inf')],
                 'val' : [34 ]
               }

    @property
    def cpo_od_sp(self):
        ''' CPO to OD spacing

        '''
        return { 'lch' : [float('inf')],
                 'val' : [20 ]
               }

    @property
    def cpo_spy(self):
        ''' CPO to CPO vertical spacing

        '''
        return { 'lch' : [float('inf')],
                 'val' : [90 ]
               }

    @property
    def cpo_h_end(self):
        ''' CPO height for substrate end

        '''
        return { 'lch' : [float('inf')],
                 'val' : [60 ]
               }

    @property
    def  md_od_exty(self):
        ''' vertical extension of MD over OD

        '''
        return { 'lch' : [float('inf')],
                 'val' : [20 ]
               }

    @property
    def md_spy(self):
        ''' vertical space bewteen MD

        '''
        return { 'lch' : [float('inf')],
                 'val' : [46 ]
               }

    @property
    def md_h_min(self):
        ''' minimum height of MD

        '''
        return { 'lch' : [float('inf')],
                 'val' : [68 ]
               }

    @property
    def  dpo_edge_spy(self):
        '''vertical space between gate PO when no CPO is used for dummy transistors
        '''
        return 0
 
    @property
    def  g_m1_dum_h(self):
        '''gate M1 dummy wire height

        '''
        return 40

    @property
    def  ds_m2_sp(self):
        '''drain/source M2 space

        '''
        return 50

    @property
    def property_dict(self):
        '''Collection dictionary of all properties of this class.
        This dictionary provides compatibility with the original BAG2 parameter calls.

        '''
        if not hasattr(self, '_property_dict'):
            #yaml_file = pkg_resources.resource_filename(__name__, os.path.join('tech_params.yaml'))
            #with open(yaml_file, 'r') as content:
               #self._process_config = {}
               #dictionary = yaml.load(content, Loader=yaml.FullLoader )

            #self._property_dict = dictionary['mos']
            self._property_dict = {}

            for key, val  in vars(type(self)).items():
                if isinstance(val,property) and key != 'property_dict':
                    self._property_dict[key] = getattr(self,key)
        return self._property_dict
