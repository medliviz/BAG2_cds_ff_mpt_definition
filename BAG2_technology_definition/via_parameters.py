"""
=========================
BAG2 Via parameter module 
=========================

Collection of Via related technology para,eters for 
BAG2 framework

Initially created by Marko Kosunen, marko.kosunen@aalto.fi, 11.04.2022.

The goal of the class is to provide a master source for the Via parameters 
and enable documentation of those parameters with docstrings.

Documentation instructions
--------------------------

Current docstring documentation style is Numpy
https://numpydoc.readthedocs.io/en/latest/format.html

"""


import os
import pkg_resources
import yaml

from BAG2_technology_template.via_parameters_template import via_parameters_template

class via_parameters(via_parameters_template):

    @property
    def via_units(self):
        '''Property that returns a dict of via units of instances of class 'via_unit'

        '''
        if not hasattr(self,'_via_units'):
            self._via_units= {}
            name = '1x'
            # Next unit
            self._via_units[name]=via_unit(name = name)
            self._via_units[name].square = {
                #Dimensions
                'dim' : [32 , 32 ],
                # via horizontal/vertical spacing
                'sp': [42 , 42 ],
                # list of valid via horizontal/vertical spacings when it's a 2x2 array
                'sp2': [[42 , 42 ]],
                # list of valid via horizontal/vertical spacings when it's a mxn array,
                # where min(m, n) >= 2, max(m, n) >= 3
                'sp3' : [[42 , 42 ]],
                # via bottom enclosure rules.  If empty, then assume it's the same
                # as top enclosure.
                'bot_enc' : None,
                # via top enclosure rules.
                'top_enc' : {
                    # via enclosure rule as function of wire width.
                    'w_list': [35, 51, 67 , float('Inf')],
                    # via enclosure rules corresponding to each width in w_list.  enc_list[idx]
                    # is used if wire width is less than or equal to w_list[idx].
                    'enc_list' : [
                        # list of valid horizontal/vertical enclosures.
                        [[40 , 0 ], [0, 40]],
                        [[2, 34 ], [34, 2]],
                        [[28, 10], [10, 28]],
                        [[18 , 18 ]],
                        ]
                }
            }
            self._via_units[name].hrect = {
                #Dimensions
                'dim' : [64 , 32 ],
                # via horizontal/vertical spacing
                'sp': [42 , 42 ],
                # list of valid via horizontal/vertical spacings when it's a 2x2 array
                'sp2': [[42 , 42]],
                # list of valid via horizontal/vertical spacings when it's a mxn array,
                # where min(m, n) >= 2, max(m, n) >= 3
                'sp3' : [[2 , 42 ]],
                # via bottom enclosure rules.  If empty, then assume it's the same
                # as top enclosure.
                'bot_enc' : None,
                # via top enclosure rules.
                'top_enc' : {
                    # via enclosure rule as function of wire width.
                    'w_list': [49 , 51 , 105 , float('Inf')],
                    # via enclosure rules corresponding to each width in w_list.  enc_list[idx]
                    # is used if wire width is less than or equal to w_list[idx].
                    'enc_list' : [
                          # list of valid horizontal/vertical enclosures.
                          [[20 , 0 ]], 
                          [[12 , 9 ]], 
                          [[10 ,10 ]], 
                          [[10 , 0 ]], 
                        ]
                }
            }
            name = '4'
            # Next unit
            self._via_units[name]=via_unit(name = name)
            self._via_units[name].square = {
                #Dimensions
                'dim' : [32 , 32 ],
                # via horizontal/vertical spacing
                'sp': [42 , 42 ],
                # list of valid via horizontal/vertical spacings when it's a 2x2 array
                'sp2': [[42 , 42 ]],
                # list of valid via horizontal/vertical spacings when it's a mxn array,
                # where min(m, n) >= 2, max(m, n) >= 3
                'sp3' : [[42 , 42 ]],
                # via bottom enclosure rules.  If empty, then assume it's the same
                # as top enclosure.
                'bot_enc' : None,
                # via top enclosure rules.
                'top_enc' : {
                    # via enclosure rule as function of wire width.
                    'w_list': [35, 51, 67 , float('Inf')],
                    # via enclosure rules corresponding to each width in w_list.  enc_list[idx]
                    # is used if wire width is less than or equal to w_list[idx].
                    'enc_list' : [
                        # list of valid horizontal/vertical enclosures.
                        [[40 , 0 ], [0, 40]],
                        [[2, 34 ], [34, 2]],
                        [[28, 10], [10, 28]],
                        [[18 , 18 ]],
                        ]
                }
            }
            self._via_units[name].hrect = {
                #Dimensions
                'dim' : [64 , 32 ],
                # via horizontal/vertical spacing
                'sp': [42 , 42 ],
                # list of valid via horizontal/vertical spacings when it's a 2x2 array
                'sp2': [[42 , 42]],
                # list of valid via horizontal/vertical spacings when it's a mxn array,
                # where min(m, n) >= 2, max(m, n) >= 3
                'sp3' : [[42 , 42 ]],
                # via bottom enclosure rules.  If empty, then assume it's the same
                # as top enclosure.
                'bot_enc' : None,
                # via top enclosure rules.
                'top_enc' : {  
                    # via enclosure rule as function of wire width.
                    'w_list': [49 , 51 , 105 , float('Inf')],
                    # via enclosure rules corresponding to each width in w_list.  enc_list[idx]
                    # is used if wire width is less than or equal to w_list[idx].
                    'enc_list' : [
                          # list of valid horizontal/vertical enclosures.
                          [[20 , 0 ]], 
                          [[12 , 9 ]], 
                          [[10 ,10 ]], 
                          [[10 , 0 ]], 
                        ]      
                }              
            }                  
            # Next unit        
            name = '2x'        
            self._via_units[name]=via_unit(name = name)
            self._via_units[name].square = {
                #Dimensions    
                'dim' : [42 , 42 ],
                # via horizontal/vertical spacing
                'sp': [62 , 62 ],
                # list of valid via horizontal/vertical spacings when it's a 2x2 array
                'sp2': [[62 , 62 ]],
                # list of valid via horizontal/vertical spacings when it's a mxn array,
                # where min(m, n) >= 2, max(m, n) >= 3
                'sp3' : [[78 , 78 ]],
                # via bottom enclosure rules.  If empty, then assume it's the same
                # as top enclosure.
                'bot_enc' : None,
                # via top enclosure rules.
                'top_enc' : {
                    # via enclosure rule as function of wire width.
                    'w_list': [ float('Inf')],
                    # via enclosure rules corresponding to each width in w_list.  enc_list[idx]
                    # is used if wire width is less than or equal to w_list[idx].
                    'enc_list' : [
                        # list of valid horizontal/vertical enclosures.
                        [[8 , 8 ]],
                        ]
                }
            }
            self._via_units[name].hrect = {
                #Dimensions
                'dim' : [64 , 32 ],
                # via horizontal/vertical spacing
                'sp': [42 , 42 ],
                # list of valid via horizontal/vertical spacings when it's a 2x2 array
                'sp2': [[42 , 42]],
                # list of valid via horizontal/vertical spacings when it's a mxn array,
                # where min(m, n) >= 2, max(m, n) >= 3
                'sp3' : [[42 , 42 ]],
                # via bottom enclosure rules.  If empty, then assume it's the same
                # as top enclosure.
                'bot_enc' : None,
                # via top enclosure rules.
                'top_enc' : {
                    # via enclosure rule as function of wire width.
                    'w_list': [49 , 51 , 105 , float('Inf')],
                    # via enclosure rules corresponding to each width in w_list.  enc_list[idx]
                    # is used if wire width is less than or equal to w_list[idx].
                    'enc_list' : [
                          # list of valid horizontal/vertical enclosures.
                          [[20 , 0 ]], 
                          [[12 , 9 ]], 
                          [[10 ,10 ]], 
                          [[10 , 0 ]], 
                        ]
                }
            }

            # Next unit
            name = '4x'
            # Next unit
            self._via_units[name]=via_unit(name = name)
            self._via_units[name].square = {
                #Dimensions
                'dim' : [42 , 42 ],
                # via horizontal/vertical spacing
                'sp': [62 , 62 ],
                # list of valid via horizontal/vertical spacings when it's a 2x2 array
                'sp2': [[62 , 62 ]],
                # list of valid via horizontal/vertical spacings when it's a mxn array,
                # where min(m, n) >= 2, max(m, n) >= 3
                'sp3' : [[78 , 78 ]],
                # via bottom enclosure rules.  If empty, then assume it's the same
                # as top enclosure.
                'bot_enc' : None,
                # via top enclosure rules.
                'top_enc' : {
                    # via enclosure rule as function of wire width.
                    'w_list': [float('Inf')],
                    # via enclosure rules corresponding to each width in w_list.  enc_list[idx]
                    # is used if wire width is less than or equal to w_list[idx].
                    'enc_list' : [
                        # list of valid horizontal/vertical enclosures.
                        [[8 , 8 ]],
                        ]
                }
            }

            # Next unit
            name = 't'
            # Next unit
            self._via_units[name]=via_unit(name = name)
            self._via_units[name].square = {
                #Dimensions
                'dim' : [42 , 42 ],
                # via horizontal/vertical spacing
                'sp': [62 , 62 ],
                # list of valid via horizontal/vertical spacings when it's a 2x2 array
                'sp2': [[62 , 62 ]],
                # list of valid via horizontal/vertical spacings when it's a mxn array,
                # where min(m, n) >= 2, max(m, n) >= 3
                'sp3' : [[78 , 78 ]],
                # via bottom enclosure rules.  If empty, then assume it's the same
                # as top enclosure.
                'bot_enc' : None,
                # via top enclosure rules.
                'top_enc' : {
                    # via enclosure rule as function of wire width.
                    'w_list': [float('Inf')],
                    # via enclosure rules corresponding to each width in w_list.  enc_list[idx]
                    # is used if wire width is less than or equal to w_list[idx].
                    'enc_list' : [
                        # list of valid horizontal/vertical enclosures.
                        [[8 , 8 ]],
                        ]
                }
            }

        return self._via_units

    @property
    def via_name(self):
        ''' Dictionary of via types between layer types.

        '''
        return {
            0 : '1x',
            1 : '1x',
            2 : '1x' ,
            3 : '4',
            4 : '2x',
            5 : '2x',
            6 : '4x',
            7 : 't' ,
        }
        
    @property
    def via_id(self):
        ''' Dictionary for mapping a via to connect of two layes to a 
        via primitive of the vendor.

        '''
        return {
            (('LiPo', 'drawing'), 'M1'): 'M1_LiPo',
            (('LiAct', 'drawing'), 'M1'): 'M1_LiAct',
            ('M1', 'M2'): 'M2_M1', 
            ('M2', 'M3'): 'M3_M2', 
            ('M3', 'M4'): 'M4_M3', 
            ('M4', 'M5'): 'M5_M4', 
            ('M5', 'M6'): 'M6_M5', 
            ('M6', 'M7'): 'M7_M6', 
            ('M7', 'MT'): 'MT_M7', 
        }

#
    @property
    def property_dict(self):
        '''Collection dictionary of all properties of this class.
        This dictionary provides compatibility with the original BAG2 parameter calls.

        '''
        if not hasattr(self, '_property_dict'):
           # yaml_file = pkg_resources.resource_filename(__name__, os.path.join('tech_params.yaml'))
           # with open(yaml_file, 'r') as content:
           #    self._process_config = {}
           #    dictionary = yaml.load(content, Loader=yaml.FullLoader )

           # self._property_dict = dictionary['via']
            self._property_dict = {}
            for key, val  in vars(type(self)).items():
                if isinstance(val,property) and key != 'property_dict':
                    self._property_dict[key] = getattr(self,key)

            for key, val  in self.via_units.items():
                    self._property_dict[key] = val.property_dict
        return self._property_dict

class via_unit:
    def __init__(self,**kwargs):
        if kwargs.get('name'):
            self._name=kwargs.get('name')
        else:
            print('Must give name for a via instance')
            
    @property
    def name(self):
        if not hasattr(self,'_name'):
            self._name = None
        return self._name

    @property
    def square(self):
        if not hasattr(self,'_square'):
            self._square = None
        return self._square

    @square.setter
    def square(self,val):
        self._square = val

    @property
    def hrect(self):
        if not hasattr(self,'_hrect'):
            self._hrect = self._square
        return self._hrect

    @hrect.setter
    def hrect(self,val):
        self._hrect = val

    @property
    def property_dict(self):
        '''Collection dictionary of all properties of this class.
        This dictionary provides compatibility with the original BAG2 parameter calls.

        '''

        if not hasattr(self, '_property_dict'):
            self._property_dict = {}

            for key, val  in vars(type(self)).items():
                if isinstance(val,property) and key != 'property_dict':
                    self._property_dict[key] = getattr(self,key)
        return self._property_dict
